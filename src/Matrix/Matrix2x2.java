package Matrix;

import Vector.Vector2D;

public class Matrix2x2 {
    // a[i,j]
    // i is an index of a row
    // j is an index of a column
    
    private final float a11;
    private final float a12;
    private final float a21;
    private final float a22;
    
    public Matrix2x2(float a11, float a21, float a12, float a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }
    
    public Matrix2x2(Vector2D column1, Vector2D column2) {
        a11 = column1.getX();
        a21 = column1.getY();
        
        a12 = column2.getX();
        a22 = column2.getY();
    }
    
    public Matrix2x2(float diag) {
        a11 = a22 = diag;
        a12 = a21 = 0.0f;
    }
    
    public float get11() {
        return a11;
    }
    
    public float get21() {
        return a21;
    }
    
    public float get12() {
        return a12;
    }
    
    public float get22() {
        return a22;
    }
    
    public Vector2D getColumn1() {
        return new Vector2D(a11, a21);
    }
    
    public Vector2D getColumn2() {
        return new Vector2D(a12, a22);
    }
    
    public Vector2D getRow1() {
        return new Vector2D(a11, a12);
    }
    
    public Vector2D getRow2() {
        return new Vector2D(a21, a22);
    }
    
    public float[] getColumnMajor() {
        return new float[]{a11, a21, a12, a22};
    }
    
    public float[] getRowMajor() {
        return new float[]{a11, a12, a21, a22};
    }
    
    public Matrix2x2 add(Matrix2x2 other) {
        return new Matrix2x2(
                a11+other.a11, a21+other.a21,
                a12+other.a12, a22+other.a22);
    }
    
    public Matrix2x2 sub(Matrix2x2 other) {
        return new Matrix2x2(
                a11-other.a11, a21-other.a21,
                a12-other.a12, a22-other.a22);
    }
    
    public Matrix2x2 mul(Matrix2x2 other) {
        return new Matrix2x2(
                a11*other.a11 + a12*other.a21,
                a21*other.a11 + a22*other.a21,
                a11*other.a12 + a12*other.a22,
                a21*other.a12 + a22*other.a22);
    }
    
    public Vector2D mul(Vector2D vec) {
        float vx = vec.getX();
        float vy = vec.getY();
        
        return new Vector2D(a11*vx + a12*vy, a21*vx + a22*vy);
    }
    
    public Matrix2x2 transpose() {
        return new Matrix2x2(a11, a12, a21, a22);
    }
    
    public float det() {
        return a11*a22 - a21*a12;
    }
}
