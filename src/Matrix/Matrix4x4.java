package Matrix;

import Vector.Vector4D;

public class Matrix4x4 {
    // a[i,j]
    // i is an index of a row
    // j is an index of a column
    
    private final float a11;
    private final float a12;
    private final float a13;
    private final float a14;
    private final float a21;
    private final float a22;
    private final float a23;
    private final float a24;
    private final float a31;
    private final float a32;
    private final float a33;
    private final float a34;
    private final float a41;
    private final float a42;
    private final float a43;
    private final float a44;
    
    public Matrix4x4(
            float a11, float a21, float a31, float a41,
            float a12, float a22, float a32, float a42,
            float a13, float a23, float a33, float a43,
            float a14, float a24, float a34, float a44) {
        this.a11 = a11;
        this.a12 = a12;
        this.a13 = a13;
        this.a14 = a14;
        this.a21 = a21;
        this.a22 = a22;
        this.a23 = a23;
        this.a24 = a24;
        this.a31 = a31;
        this.a32 = a32;
        this.a33 = a33;
        this.a34 = a34;
        this.a41 = a41;
        this.a42 = a42;
        this.a43 = a43;
        this.a44 = a44;
    }
    
    public Matrix4x4(Vector4D column1, Vector4D column2, Vector4D column3, Vector4D column4) {
        a11 = column1.getX();
        a21 = column1.getY();
        a31 = column1.getZ();
        a41 = column1.getW();
        
        a12 = column2.getX();
        a22 = column2.getY();
        a32 = column2.getZ();
        a42 = column2.getW();
        
        a13 = column3.getX();
        a23 = column3.getY();
        a33 = column3.getZ();
        a43 = column3.getW();
        
        a14 = column4.getX();
        a24 = column4.getY();
        a34 = column4.getZ();
        a44 = column4.getW();
    }
    
    public Matrix4x4(float diag) {
        a11 = a22 = a33 = a44 = diag;
        a12 = a21 = a13 = a31 = a23 = a32 = a14 = a41 = a24 = a42 = a34 = a43 = 0.0f;
    }
    
    public float get11() {
        return a11;
    }
    
    public float get21() {
        return a21;
    }
    
    public float get31() {
        return a31;
    }
    
    public float get41() {
        return a41;
    }
    
    public float get12() {
        return a12;
    }
    
    public float get22() {
        return a22;
    }
    
    public float get32() {
        return a32;
    }
    
    public float get42() {
        return a42;
    }
    
    public float get13() {
        return a13;
    }
    
    public float get23() {
        return a23;
    }
    
    public float get33() {
        return a33;
    }
    
    public float get43() {
        return a43;
    }
    
    public float get14() {
        return a14;
    }
    
    public float get24() {
        return a24;
    }
    
    public float get34() {
        return a34;
    }
    
    public float get44() {
        return a44;
    }
    
    public Vector4D getColumn1() {
        return new Vector4D(a11, a21, a31, a41);
    }
    
    public Vector4D getColumn2() {
        return new Vector4D(a12, a22, a32, a42);
    }
    
    public Vector4D getColumn3() {
        return new Vector4D(a13, a23, a33, a43);
    }
    
    public Vector4D getColumn4() {
        return new Vector4D(a14, a24, a34, a44);
    }
    
    public Vector4D getRow1() {
        return new Vector4D(a11, a12, a13, a14);
    }
    
    public Vector4D getRow2() {
        return new Vector4D(a21, a22, a23, a24);
    }
    
    public Vector4D getRow3() {
        return new Vector4D(a31, a32, a33, a34);
    }
    
    public Vector4D getRow4() {
        return new Vector4D(a41, a42, a43, a44);
    }
    
    public float[] getColumnMajor() {
        return new float[]{
            a11, a21, a31, a41,
            a12, a22, a32, a42,
            a13, a23, a33, a43,
            a14, a24, a34, a44};
    }
    
    public float[] getRowMajor() {
        return new float[]{
            a11, a12, a13, a14,
            a21, a22, a23, a24,
            a31, a32, a33, a34,
            a41, a42, a43, a44};
    }
    
    public Matrix4x4 add(Matrix4x4 other) {
        return new Matrix4x4(
                a11+other.a11, a21+other.a21, a31+other.a31, a41+other.a41,
                a12+other.a12, a22+other.a22, a32+other.a32, a42+other.a42,
                a13+other.a13, a23+other.a23, a33+other.a33, a43+other.a43,
                a14+other.a14, a24+other.a24, a34+other.a34, a44+other.a44);
    }
    
    public Matrix4x4 sub(Matrix4x4 other) {
        return new Matrix4x4(
                a11-other.a11, a21-other.a21, a31-other.a31, a41-other.a41,
                a12-other.a12, a22-other.a22, a32-other.a32, a42-other.a42,
                a13-other.a13, a23-other.a23, a33-other.a33, a43-other.a43,
                a14-other.a14, a24-other.a24, a34-other.a34, a44-other.a44);
    }
    
    public Matrix4x4 mul(Matrix4x4 other) {
        return new Matrix4x4(
                a11*other.a11 + a12*other.a21 + a13*other.a31 + a14*other.a41,
                a21*other.a11 + a22*other.a21 + a23*other.a31 + a24*other.a41,
                a31*other.a11 + a32*other.a21 + a33*other.a31 + a34*other.a41,
                a41*other.a11 + a42*other.a21 + a43*other.a31 + a44*other.a41,
                
                a11*other.a12 + a12*other.a22 + a13*other.a32 + a14*other.a42,
                a21*other.a12 + a22*other.a22 + a23*other.a32 + a24*other.a42,
                a31*other.a12 + a32*other.a22 + a33*other.a32 + a34*other.a42,
                a41*other.a12 + a42*other.a22 + a43*other.a32 + a44*other.a42,
        
                a11*other.a13 + a12*other.a23 + a13*other.a33 + a14*other.a43,
                a21*other.a13 + a22*other.a23 + a23*other.a33 + a24*other.a43,
                a31*other.a13 + a32*other.a23 + a33*other.a33 + a34*other.a43,
                a41*other.a13 + a42*other.a23 + a43*other.a33 + a44*other.a43,
                
                a11*other.a14 + a12*other.a24 + a13*other.a34 + a14*other.a44,
                a21*other.a14 + a22*other.a24 + a23*other.a34 + a24*other.a44,
                a31*other.a14 + a32*other.a24 + a33*other.a34 + a34*other.a44,
                a41*other.a14 + a42*other.a24 + a43*other.a34 + a44*other.a44);
    }
    
    public Vector4D mul(Vector4D vec) {
        float vx = vec.getX();
        float vy = vec.getY();
        float vz = vec.getZ();
        float vw = vec.getW();
        
        return new Vector4D(
                a11*vx + a12*vy + a13*vz + a14*vw,
                a21*vx + a22*vy + a23*vz + a24*vw,
                a31*vx + a32*vy + a33*vz + a34*vw,
                a41*vx + a42*vy + a43*vz + a44*vw);
    }
    
    public Matrix4x4 transpose() {
        return new Matrix4x4(
                a11, a12, a13, a14,
                a21, a22, a23, a24,
                a31, a32, a33, a34,
                a41, a42, a43, a44);
    }
    
    public float det() {
        return a11*(a22*a33*a44 + a23*a34*a42 + a24*a32*a43 - a24*a33*a42 - a23*a32*a44 - a22*a34*a43) -
               a21*(a12*a33*a44 + a13*a34*a42 + a14*a32*a43 - a14*a33*a42 - a13*a32*a44 - a12*a34*a43) +
               a31*(a12*a23*a44 + a13*a24*a42 + a14*a22*a43 - a14*a23*a42 - a13*a22*a44 - a12*a24*a43) -
               a41*(a12*a23*a34 + a13*a24*a32 + a14*a22*a33 - a14*a23*a32 - a13*a22*a34 - a12*a24*a33);
    }
}
