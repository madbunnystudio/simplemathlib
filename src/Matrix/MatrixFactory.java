package Matrix;

import Vector.Vector3D;

public class MatrixFactory {
    public static Matrix4x4 getPerspective(float left, float right, float bottom,
            float top, float near, float far) {
        float tmb = top - bottom;
        float rml = right - left;
        float fmn = far - near;
        
        return new Matrix4x4(
                2.0f*near/rml, 0.0f, 0.0f, 0.0f,
                0.0f, 2.0f*near/tmb, 0.0f, 0.0f,
                (right+left)/rml, (top+bottom)/tmb, -(far+near)/fmn, -1.0f,
                0.0f, 0.0f, -2.0f*far*near/fmn, 0.0f);
    }
    
    public static Matrix4x4 getPerspective(float fov, float aspectRatio,
            float zNear, float zFar) {
        float f = 1.0f / (float)Math.tan((float)(fov * (float)Math.PI / 360.0f));
        float A = (zFar + zNear) / (zNear - zFar);
        float B = (2.0f * zFar * zNear) / (zNear - zFar);
        
        return new Matrix4x4(
                f/aspectRatio, 0.0f, 0.0f, 0.0f,
                0.0f, f, 0.0f, 0.0f,
                0.0f, 0.0f, A, -1.0f,
                0.0f, 0.0f, B, 0.0f);
    }
    
    public static Matrix4x4 getOrthographic(float left,float right, float bottom, float top) {
        float rml = right - left;
        float tmb = top - bottom;
        
        return new Matrix4x4(
                2.0f/rml, 0.0f, 0.0f, 0.0f,
                0.0f, 2.0f/tmb, 0.0f, 0.0f,
                0.0f, 0.0f, -1.0f, 0.0f,
                -(right+left)/rml, -(top+bottom)/tmb, 0.0f, 1.0f);
    }
    
    public static Matrix4x4 getTranslation(float x, float y, float z) {
        return new Matrix4x4(
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                x, y, z, 1.0f);
    }
    
    public static Matrix4x4 getTranslation(Vector3D xyz) {
        return getTranslation(xyz.getX(), xyz.getY(), xyz.getZ());
    }
    
    public static Matrix4x4 getScale(float x, float y, float z) { 
        return new Matrix4x4(
                x, 0.0f, 0.0f, 0.0f,
                0.0f, y, 0.0f, 0.0f,
                0.0f, 0.0f, z, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    public static Matrix4x4 getScale(Vector3D xyz) {
        return getScale(xyz.getX(), xyz.getY(), xyz.getZ());
    }
    
    public static Matrix4x4 getRotationX(float angle) {
        float cosa = (float)Math.cos((double)angle);
        float sina = (float)Math.sin((double)angle);
        
        return new Matrix4x4(
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, cosa, sina, 0.0f,
                0.0f, -sina, cosa, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    public static Matrix4x4 getRotationY(float angle) {
        float cosa = (float)Math.cos((double)angle);
        float sina = (float)Math.sin((double)angle);
        
        return new Matrix4x4(
                cosa, 0.0f, -sina, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                sina, 0.0f, cosa, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    public static Matrix4x4 getRotationZ(float angle) {
        float cosa = (float)Math.cos((double)angle);
        float sina = (float)Math.sin((double)angle);
        
        return new Matrix4x4(
                cosa, sina, 0.0f, 0.0f,
                -sina, cosa, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    // Rotation along an arbitrary axis
    // Vector (x, y, z) must be normalized
    public static Matrix4x4 getRotationAlongAxis(float x, float y, float z) {
        float A = (float)Math.cos((float)x);
        float B = (float)Math.sin((float)x);
        float C = (float)Math.cos((float)y);
        float D = (float)Math.sin((float)y);
        float E = (float)Math.cos((float)z);
        float F = (float)Math.sin((float)z);
        float AD = A*D;
        float BD = B*D;
        
        return new Matrix4x4(
                C*E, BD*E + A*F, -AD*E + B*F, 0.0f,
                -C*F, -BD*F + A*E, AD*F + B*E, 0.0f,
                D, -B*C, A*C, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    // Vector xyz must be normalized
    public static Matrix4x4 getRotationAlongAxis(Vector3D xyz) {
        return getRotationAlongAxis(xyz.getX(), xyz.getY(), xyz.getZ());
    }
}
