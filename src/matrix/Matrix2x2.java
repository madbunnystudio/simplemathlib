package matrix;

import vector.Vector2D;

public class Matrix2x2 {
    // a[i,j]
    // i is an index of a row
    // j is an index of a column
    
    public final float a11;
    public final float a12;
    public final float a21;
    public final float a22;
    
    public Matrix2x2(float a11, float a21, float a12, float a22) {
        this.a11 = a11;
        this.a12 = a12;
        this.a21 = a21;
        this.a22 = a22;
    }
    
    public Matrix2x2(Vector2D column1, Vector2D column2) {
        a11 = column1.x;
        a21 = column1.y;
        
        a12 = column2.x;
        a22 = column2.y;
    }
    
    public Matrix2x2(float diag) {
        a11 = a22 = diag;
        a12 = a21 = 0.0f;
    }
    
    public Vector2D getColumn1() {
        return new Vector2D(a11, a21);
    }
    
    public Vector2D getColumn2() {
        return new Vector2D(a12, a22);
    }
    
    public Vector2D getRow1() {
        return new Vector2D(a11, a12);
    }
    
    public Vector2D getRow2() {
        return new Vector2D(a21, a22);
    }
    
    public float[] getColumnMajor() {
        return new float[]{a11, a21, a12, a22};
    }
    
    public float[] getRowMajor() {
        return new float[]{a11, a12, a21, a22};
    }
    
    public Matrix2x2 add(Matrix2x2 other) {
        return new Matrix2x2(
                a11+other.a11, a21+other.a21,
                a12+other.a12, a22+other.a22);
    }
    
    public Matrix2x2 sub(Matrix2x2 other) {
        return new Matrix2x2(
                a11-other.a11, a21-other.a21,
                a12-other.a12, a22-other.a22);
    }
    
    public Matrix2x2 mul(Matrix2x2 other) {
        return new Matrix2x2(
                a11*other.a11 + a12*other.a21,
                a21*other.a11 + a22*other.a21,
                a11*other.a12 + a12*other.a22,
                a21*other.a12 + a22*other.a22);
    }
    
    public Vector2D mul(Vector2D vec) {
        float vx = vec.x;
        float vy = vec.y;
        
        return new Vector2D(a11*vx + a12*vy, a21*vx + a22*vy);
    }
    
    public Matrix2x2 transpose() {
        return new Matrix2x2(a11, a12, a21, a22);
    }
    
    public float det() {
        return a11*a22 - a21*a12;
    }
    
    public Matrix2x2 inverse() {
        float d = det();
        return new Matrix2x2(a22/d, -a12/d, -a21/d, a11/d);
    }
}
