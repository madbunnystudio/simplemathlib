package matrix;

import vector.Vector3D;

public class Matrix3x3 {
    // a[i,j]
    // i is an index of a row
    // j is an index of a column
    
    public final float a11;
    public final float a12;
    public final float a13;
    public final float a21;
    public final float a22;
    public final float a23;
    public final float a31;
    public final float a32;
    public final float a33;
    
    public Matrix3x3(
            float a11, float a21, float a31,
            float a12, float a22, float a32,
            float a13, float a23, float a33) {
        this.a11 = a11;
        this.a12 = a12;
        this.a13 = a13;
        this.a21 = a21;
        this.a22 = a22;
        this.a23 = a23;
        this.a31 = a31;
        this.a32 = a32;
        this.a33 = a33;
    }
    
    public Matrix3x3(Vector3D column1, Vector3D column2, Vector3D column3) {
        a11 = column1.x;
        a21 = column1.y;
        a31 = column1.z;
        
        a12 = column2.x;
        a22 = column2.y;
        a32 = column2.z;
        
        a13 = column3.x;
        a23 = column3.y;
        a33 = column3.z;
    }
    
    public Matrix3x3(float diag) {
        a11 = a22 = a33 = diag;
        a12 = a21 = a13 = a31 = a23 = a32 = 0.0f;
    }
    
    public Vector3D getColumn1() {
        return new Vector3D(a11, a21, a31);
    }
    
    public Vector3D getColumn2() {
        return new Vector3D(a12, a22, a32);
    }
    
    public Vector3D getColumn3() {
        return new Vector3D(a13, a23, a33);
    }
    
    public Vector3D getRow1() {
        return new Vector3D(a11, a12, a13);
    }
    
    public Vector3D getRow2() {
        return new Vector3D(a21, a22, a23);
    }
    
    public Vector3D getRow3() {
        return new Vector3D(a31, a32, a33);
    }
    
    public float[] getColumnMajor() {
        return new float[]{a11, a21, a31, a12, a22, a32, a13, a23, a33};
    }
    
    public float[] getRowMajor() {
        return new float[]{a11, a12, a13, a21, a22, a23, a31, a32, a33};
    }
    
    public Matrix3x3 add(Matrix3x3 other) {
        return new Matrix3x3(
                a11+other.a11, a21+other.a21, a31+other.a31,
                a12+other.a12, a22+other.a22, a32+other.a32,
                a13+other.a13, a23+other.a23, a33+other.a33);
    }
    
    public Matrix3x3 sub(Matrix3x3 other) {
        return new Matrix3x3(
                a11-other.a11, a21-other.a21, a31-other.a31,
                a12-other.a12, a22-other.a22, a32-other.a32,
                a13-other.a13, a23-other.a23, a33-other.a33);
    }
    
    public Matrix3x3 mul(Matrix3x3 other) {
        return new Matrix3x3(
                a11*other.a11 + a12*other.a21 + a13*other.a31,
                a21*other.a11 + a22*other.a21 + a23*other.a31,
                a31*other.a11 + a32*other.a21 + a33*other.a31,
                
                a11*other.a12 + a12*other.a22 + a13*other.a32,
                a21*other.a12 + a22*other.a22 + a23*other.a32,
                a31*other.a12 + a32*other.a22 + a33*other.a32,
        
                a11*other.a13 + a12*other.a23 + a13*other.a33,
                a21*other.a13 + a22*other.a23 + a23*other.a33,
                a31*other.a13 + a32*other.a23 + a33*other.a33);
    }
    
    public Vector3D mul(Vector3D vec) {
        float vx = vec.x;
        float vy = vec.y;
        float vz = vec.z;
        
        return new Vector3D(
                a11*vx + a12*vy + a13*vz,
                a21*vx + a22*vy + a23*vz,
                a31*vx + a32*vy + a33*vz);
    }
    
    public Matrix3x3 transpose() {
        return new Matrix3x3(a11, a12, a13, a21, a22, a23, a31, a32, a33);
    }
    
    public float det() {
        return a11*a22*a33 + a12*a23*a31 + a13*a21*a32 -
                a13*a22*a31 - a12*a21*a33 - a11*a23*a32;
    }
    
    public Matrix3x3 inverse() {
        float d = det();
        
        float M11 = (a22*a33 - a32*a23)/d;
        float M12 = -(a21*a33 - a31*a23)/d;
        float M13 = (a21*a32 - a31*a22)/d;
        float M21 = -(a12*a33 - a32*a13)/d;
        float M22 = (a11*a33 - a31*a13)/d;
        float M23 = -(a11*a32 - a12*a31)/d;
        float M31 = (a12*a23 - a22*a13)/d;
        float M32 = -(a11*a23 - a21*a13)/d;
        float M33 = (a11*a22 - a21*a12)/d;
        
        return new Matrix3x3(
                M11, M12, M13,
                M21, M22, M23,
                M31, M32, M33);
    }
    
    static float det(
            float a11, float a21, float a31,
            float a12, float a22, float a32,
            float a13, float a23, float a33) {
        return a11*a22*a33 + a12*a23*a31 + a13*a21*a32 -
                a13*a22*a31 - a12*a21*a33 - a11*a23*a32;
    }
}
