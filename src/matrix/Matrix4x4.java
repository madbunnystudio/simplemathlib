package matrix;

import vector.Vector4D;

public class Matrix4x4 {
    // a[i,j]
    // i is an index of a row
    // j is an index of a column
    
    public final float a11;
    public final float a12;
    public final float a13;
    public final float a14;
    public final float a21;
    public final float a22;
    public final float a23;
    public final float a24;
    public final float a31;
    public final float a32;
    public final float a33;
    public final float a34;
    public final float a41;
    public final float a42;
    public final float a43;
    public final float a44;
    
    public Matrix4x4(
            float a11, float a21, float a31, float a41,
            float a12, float a22, float a32, float a42,
            float a13, float a23, float a33, float a43,
            float a14, float a24, float a34, float a44) {
        this.a11 = a11;
        this.a12 = a12;
        this.a13 = a13;
        this.a14 = a14;
        this.a21 = a21;
        this.a22 = a22;
        this.a23 = a23;
        this.a24 = a24;
        this.a31 = a31;
        this.a32 = a32;
        this.a33 = a33;
        this.a34 = a34;
        this.a41 = a41;
        this.a42 = a42;
        this.a43 = a43;
        this.a44 = a44;
    }
    
    public Matrix4x4(Vector4D column1, Vector4D column2, Vector4D column3, Vector4D column4) {
        a11 = column1.x;
        a21 = column1.y;
        a31 = column1.z;
        a41 = column1.w;
        
        a12 = column2.x;
        a22 = column2.y;
        a32 = column2.z;
        a42 = column2.w;
        
        a13 = column3.x;
        a23 = column3.y;
        a33 = column3.z;
        a43 = column3.w;
        
        a14 = column4.x;
        a24 = column4.y;
        a34 = column4.z;
        a44 = column4.w;
    }
    
    public Matrix4x4(float diag) {
        a11 = a22 = a33 = a44 = diag;
        a12 = a21 = a13 = a31 = a23 = a32 = a14 = a41 = a24 = a42 = a34 = a43 = 0.0f;
    }
    
    public Vector4D getColumn1() {
        return new Vector4D(a11, a21, a31, a41);
    }
    
    public Vector4D getColumn2() {
        return new Vector4D(a12, a22, a32, a42);
    }
    
    public Vector4D getColumn3() {
        return new Vector4D(a13, a23, a33, a43);
    }
    
    public Vector4D getColumn4() {
        return new Vector4D(a14, a24, a34, a44);
    }
    
    public Vector4D getRow1() {
        return new Vector4D(a11, a12, a13, a14);
    }
    
    public Vector4D getRow2() {
        return new Vector4D(a21, a22, a23, a24);
    }
    
    public Vector4D getRow3() {
        return new Vector4D(a31, a32, a33, a34);
    }
    
    public Vector4D getRow4() {
        return new Vector4D(a41, a42, a43, a44);
    }
    
    public float[] getColumnMajor() {
        return new float[]{
            a11, a21, a31, a41,
            a12, a22, a32, a42,
            a13, a23, a33, a43,
            a14, a24, a34, a44};
    }
    
    public float[] getRowMajor() {
        return new float[]{
            a11, a12, a13, a14,
            a21, a22, a23, a24,
            a31, a32, a33, a34,
            a41, a42, a43, a44};
    }
    
    public Matrix4x4 add(Matrix4x4 other) {
        return new Matrix4x4(
                a11+other.a11, a21+other.a21, a31+other.a31, a41+other.a41,
                a12+other.a12, a22+other.a22, a32+other.a32, a42+other.a42,
                a13+other.a13, a23+other.a23, a33+other.a33, a43+other.a43,
                a14+other.a14, a24+other.a24, a34+other.a34, a44+other.a44);
    }
    
    public Matrix4x4 sub(Matrix4x4 other) {
        return new Matrix4x4(
                a11-other.a11, a21-other.a21, a31-other.a31, a41-other.a41,
                a12-other.a12, a22-other.a22, a32-other.a32, a42-other.a42,
                a13-other.a13, a23-other.a23, a33-other.a33, a43-other.a43,
                a14-other.a14, a24-other.a24, a34-other.a34, a44-other.a44);
    }
    
    public Matrix4x4 mul(Matrix4x4 other) {
        return new Matrix4x4(
                a11*other.a11 + a12*other.a21 + a13*other.a31 + a14*other.a41,
                a21*other.a11 + a22*other.a21 + a23*other.a31 + a24*other.a41,
                a31*other.a11 + a32*other.a21 + a33*other.a31 + a34*other.a41,
                a41*other.a11 + a42*other.a21 + a43*other.a31 + a44*other.a41,
                
                a11*other.a12 + a12*other.a22 + a13*other.a32 + a14*other.a42,
                a21*other.a12 + a22*other.a22 + a23*other.a32 + a24*other.a42,
                a31*other.a12 + a32*other.a22 + a33*other.a32 + a34*other.a42,
                a41*other.a12 + a42*other.a22 + a43*other.a32 + a44*other.a42,
        
                a11*other.a13 + a12*other.a23 + a13*other.a33 + a14*other.a43,
                a21*other.a13 + a22*other.a23 + a23*other.a33 + a24*other.a43,
                a31*other.a13 + a32*other.a23 + a33*other.a33 + a34*other.a43,
                a41*other.a13 + a42*other.a23 + a43*other.a33 + a44*other.a43,
                
                a11*other.a14 + a12*other.a24 + a13*other.a34 + a14*other.a44,
                a21*other.a14 + a22*other.a24 + a23*other.a34 + a24*other.a44,
                a31*other.a14 + a32*other.a24 + a33*other.a34 + a34*other.a44,
                a41*other.a14 + a42*other.a24 + a43*other.a34 + a44*other.a44);
    }
    
    public Vector4D mul(Vector4D vec) {
        float vx = vec.x;
        float vy = vec.y;
        float vz = vec.z;
        float vw = vec.w;
        
        return new Vector4D(
                a11*vx + a12*vy + a13*vz + a14*vw,
                a21*vx + a22*vy + a23*vz + a24*vw,
                a31*vx + a32*vy + a33*vz + a34*vw,
                a41*vx + a42*vy + a43*vz + a44*vw);
    }
    
    public Matrix4x4 transpose() {
        return new Matrix4x4(
                a11, a12, a13, a14,
                a21, a22, a23, a24,
                a31, a32, a33, a34,
                a41, a42, a43, a44);
    }
    
    public float det() {
        return a11*(a22*a33*a44 + a23*a34*a42 + a24*a32*a43 - a24*a33*a42 - a23*a32*a44 - a22*a34*a43) -
               a21*(a12*a33*a44 + a13*a34*a42 + a14*a32*a43 - a14*a33*a42 - a13*a32*a44 - a12*a34*a43) +
               a31*(a12*a23*a44 + a13*a24*a42 + a14*a22*a43 - a14*a23*a42 - a13*a22*a44 - a12*a24*a43) -
               a41*(a12*a23*a34 + a13*a24*a32 + a14*a22*a33 - a14*a23*a32 - a13*a22*a34 - a12*a24*a33);
    }
    
    public Matrix4x4 inverse() {
        float d = det();
        
        float M11 = Matrix3x3.det(a22, a32, a42, a23, a33, a43, a24, a34, a44)/d;
        float M21 = -Matrix3x3.det(a12, a32, a42, a13, a33, a43, a14, a34, a44)/d;
        float M31 = Matrix3x3.det(a12, a22, a42, a13, a23, a43, a14, a24, a44)/d;
        float M41 = -Matrix3x3.det(a12, a22, a32, a13, a23, a33, a14, a24, a34)/d;
        
        float M12 = -Matrix3x3.det(a21, a31, a41, a23, a33, a43, a24, a34, a44)/d;
        float M22 = Matrix3x3.det(a11, a31, a41, a13, a33, a43, a14, a34, a44)/d;
        float M32 = -Matrix3x3.det(a11, a21, a41, a13, a23, a43, a14, a24, a44)/d;
        float M42 = Matrix3x3.det(a11, a21, a31, a13, a23, a33, a14, a24, a34)/d;
        
        float M13 = Matrix3x3.det(a21, a31, a41, a22, a32, a42, a24, a34, a44)/d;
        float M23 = -Matrix3x3.det(a11, a31, a41, a12, a32, a42, a14, a34, a44)/d;
        float M33 = Matrix3x3.det(a11, a21, a41, a12, a22, a42, a14, a24, a44)/d;
        float M43 = -Matrix3x3.det(a11, a21, a31, a12, a22, a32, a14, a24, a34)/d;
        
        float M14 = -Matrix3x3.det(a21, a31, a41, a22, a32, a42, a23, a33, a43)/d;
        float M24 = Matrix3x3.det(a11, a31, a41, a12, a32, a42, a13, a33, a43)/d;
        float M34 = -Matrix3x3.det(a11, a21, a41, a12, a22, a42, a13, a23, a43)/d;
        float M44 = Matrix3x3.det(a11, a21, a31, a12, a22, a32, a13, a23, a33)/d;
        
        return new Matrix4x4(
                M11, M12, M13, M14,
                M21, M22, M23, M24,
                M31, M32, M33, M34,
                M41, M42, M43, M44);
    }
}
