package Vector;

public class Vector4D {
    private final float x;
    private final float y;
    private final float z;
    private final float w;
    
    public Vector4D(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    
    public Vector4D(Vector2D xy, float z, float w) {
        this.x = xy.getX();
        this.y = xy.getY();
        this.z = z;
        this.w = w;
    }
    
    public Vector4D(float x, Vector2D yz, float w) {
        this.x = x;
        this.y = yz.getX();
        this.z = yz.getY();
        this.w = w;
    }
    
    public Vector4D(float x, float y, Vector2D zw) {
        this.x = x;
        this.y = y;
        this.z = zw.getX();
        this.w = zw.getY();
    }
    
    public Vector4D(float x, Vector3D yzw) {
        this.x = x;
        this.y = yzw.getX();
        this.z = yzw.getY();
        this.w = yzw.getZ();
    }
    
    public Vector4D(Vector3D xyz, float w) {
        this.x = xyz.getX();
        this.y = xyz.getY();
        this.z = xyz.getZ();
        this.w = w;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float getZ() {
        return z;
    }
    
    public float getW() {
        return w;
    }
    
    public float[] getXYZW() {
        return new float[]{x, y, z, w};
    }
    
    public Vector4D add(Vector4D other) {
        return new Vector4D(x + other.x, y + other.y, z + other.z, w + other.w);
    }
    
    public Vector4D sub(Vector4D other) {
        return new Vector4D(x - other.x, y - other.y, z - other.z, w - other.w);
    }
    
    public Vector4D mul(float scalar) {
        return new Vector4D(x*scalar, y*scalar, z*scalar, w*scalar);
    }
    
    // Dot product
    public float dot(Vector4D other) {
        return x*other.x + y*other.y + z*other.z + w*other.w;
    }
    
    public float length() {
        return (float)Math.sqrt((float)this.dot(this));
    }
    
    public Vector4D norm() {
        return this.mul(1.0f/this.length());
    }
}
