package Vector;

public class Vector2D {
    private final float x;
    private final float y;
    
    public Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float[] getXY() {
        return new float[]{x, y};
    }
    
    public Vector2D add(Vector2D other) {
        return new Vector2D(x + other.x, y + other.y);
    }
    
    public Vector2D sub(Vector2D other) {
        return new Vector2D(x - other.x, y - other.y);
    }
    
    public Vector2D mul(float scalar) {
        return new Vector2D(x*scalar, y*scalar);
    }
    
    // Dot product
    public float dot(Vector2D other) {
        return x*other.x + y*other.y;
    }
    
    public float length() {
        return (float)Math.sqrt((float)this.dot(this));
    }
    
    public Vector2D norm() {
        return this.mul(1.0f/this.length());
    }
}
