package vector;

public class Vector4D {
    public final float x;
    public final float y;
    public final float z;
    public final float w;
    
    public Vector4D(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }
    
    public Vector4D(Vector2D xy, float z, float w) {
        this.x = xy.x;
        this.y = xy.y;
        this.z = z;
        this.w = w;
    }
    
    public Vector4D(float x, Vector2D yz, float w) {
        this.x = x;
        this.y = yz.x;
        this.z = yz.y;
        this.w = w;
    }
    
    public Vector4D(float x, float y, Vector2D zw) {
        this.x = x;
        this.y = y;
        this.z = zw.x;
        this.w = zw.y;
    }
    
    public Vector4D(float x, Vector3D yzw) {
        this.x = x;
        this.y = yzw.x;
        this.z = yzw.y;
        this.w = yzw.z;
    }
    
    public Vector4D(Vector3D xyz, float w) {
        this.x = xyz.x;
        this.y = xyz.y;
        this.z = xyz.z;
        this.w = w;
    }
    
    public Vector4D add(Vector4D other) {
        return new Vector4D(x + other.x, y + other.y, z + other.z, w + other.w);
    }
    
    public Vector4D sub(Vector4D other) {
        return new Vector4D(x - other.x, y - other.y, z - other.z, w - other.w);
    }
    
    public Vector4D mul(float scalar) {
        return new Vector4D(x*scalar, y*scalar, z*scalar, w*scalar);
    }
    
    // Dot product
    public float dot(Vector4D other) {
        return x*other.x + y*other.y + z*other.z + w*other.w;
    }
    
    public float length() {
        return (float)Math.sqrt((float)this.dot(this));
    }
    
    public Vector4D norm() {
        return this.mul(1.0f/this.length());
    }
}
