package vector;

public class Vector3D {
    public final float x;
    public final float y;
    public final float z;
    
    public Vector3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Vector3D(Vector2D xy, float z) {
        this.x = xy.x;
        this.y = xy.y;
        this.z = z;
    }
    
    public Vector3D(float x, Vector2D yz) {
        this.x = x;
        this.y = yz.x;
        this.z = yz.y;
    }
    
    public Vector3D add(Vector3D other) {
        return new Vector3D(x + other.x, y + other.y, z + other.z);
    }
    
    public Vector3D sub(Vector3D other) {
        return new Vector3D(x - other.x, y - other.y, z - other.z);
    }
    
    public Vector3D mul(float scalar) {
        return new Vector3D(x*scalar, y*scalar, z*scalar);
    }
    
    // Dot product
    public float dot(Vector3D other) {
        return x*other.x + y*other.y + z*other.z;
    }
    
    // Cross product
    public Vector3D cross(Vector3D other) {
        return new Vector3D(
                y*other.z - z*other.y,
                z*other.x - x*other.z,
                x*other.y - y*other.x);
    }
    
    public float length() {
        return (float)Math.sqrt((float)this.dot(this));
    }
    
    public Vector3D norm() {
        return this.mul(1.0f/this.length());
    }
}

