package vector;

public class Vector2D {
    public final float x;
    public final float y;
    
    public Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    public Vector2D add(Vector2D other) {
        return new Vector2D(x + other.x, y + other.y);
    }
    
    public Vector2D sub(Vector2D other) {
        return new Vector2D(x - other.x, y - other.y);
    }
    
    public Vector2D mul(float scalar) {
        return new Vector2D(x*scalar, y*scalar);
    }
    
    // Dot product
    public float dot(Vector2D other) {
        return x*other.x + y*other.y;
    }
    
    public float length() {
        return (float)Math.sqrt((float)this.dot(this));
    }
    
    public Vector2D norm() {
        return this.mul(1.0f/this.length());
    }
}
